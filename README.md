# README for imx8_pinlist

For the successful development of an i.MX8 based application, it's essential to know the pin assignment
of the modules board-to-board connector and the i.MX processor chip. This project hosts the pin assignment
lists for all the various congatec i.MX8 based products.

The assignment list for each product is provided on an individual branch. The name of the branch consist
of the prefix "cgt" followed by the congatec project code followed by "_pinlist".

For instance, in order to checkout the pin assignment list for conga-SMX8-X (aka SX8X), the branchname
is "cgtsx8x_pinlist".

Currently, the assignment lists for the following products are available:

|product|project code|form factor|processor|branchname|filename|
|-------|------------|-----------|---------|----------|--------|
|conga-QMX8|QMX8|QSeven|i.MX8QM|cgtqmx8_pinlist|cgtqmx8_pin_connection.xlsx|
|conga-QMX8-X|QX8X|QSeven|i.MX8X|cgtqx8x_pinlist|cgtqx8x_pin_connection.xlsx|
|conga-SMX8|SMX8|SMARC|i.MX8QM|cgtsmx8_pinlist|cgtsmx8_pin_connection.xlsx|
|conga-SMX8-X|SX8X|SMARC|i.MX8X|cgtsx8x_pinlist|cgtsx8x_pin_connection.xlsx|
|conga-SMX8-Mini|SX8M|SMARC|i.MX8MM|cgtsx8m_pinlist|cgtsx8m_pin_connection.xlsx|
|conga-SMX8-Plus|SX8P|SMARC|i.MX8MP|cgtsx8p_pinlist|cgtsx8p_pin_connection.xlsx|
|conga-QMX8-Plus|QX8P|QSeven|i.MX8MP|cgtqx8p_pinlist|cgtqx8p_pin_connection.xlsx|

In order to select the correct list, just checkout the branch for the desired product.
The assignment list is in Microsoft Excel file format.  

(c) 2024, congatec GmbH
